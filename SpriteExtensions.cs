﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Merthsoft.Extensions {
	[Obsolete()]
	public static class SpriteExtensions {
		/// <summary>
		/// Swaps two items.
		/// </summary>
		/// <typeparam name="T">The type of the items we're swapping.</typeparam>
		/// <param name="item1">The first item.</param>
		/// <param name="item2">The second item.</param>
		public static void Swap<T>(ref T item1, ref T item2) {
			T temp = item1;
			item1 = item2;
			item2 = temp;
		}

		/// <summary>
		/// Plots a point to the sprite.
		/// </summary>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x">The X coordinate to plot.</param>
		/// <param name="y">The Y coordinate to plot.</param>
		/// <param name="color">The color to draw.</param>
		/// <param name="plotWidth">The pen width.</param>
		public static void Plot(this int[,] sprite, int x, int y, int color, int plotWidth = 1) {
			if (plotWidth == 1) {
				if (x >= 0 && y >= 0 && x < sprite.GetLength(0) && y < sprite.GetLength(1)) {
					sprite[x, y] = color;
				}
			} else {
				int r = plotWidth / 2;
				sprite.DrawRectangle(x - r, y - r, x + 1, y + 1, color, 1, true);
			}
		}

		/// <summary>
		/// Draws a line to the sprite.
		/// </summary>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x1">The X coordinate of one end point.</param>
		/// <param name="y1">The Y coordinate of one end point.</param>
		/// <param name="x2">The X coordinate of the second end point.</param>
		/// <param name="y2">The Y coordinate of the second end point.</param>
		/// <param name="color">The color to draw.</param>
		/// <param name="plotWidth">The pen width.</param>
		public static void DrawLine(this int[,] sprite, int x1, int y1, int x2, int y2, int color, int plotWidth = 1) {
			   int deltaX = (int)Math.Abs(x1-x2);
			   int deltaY = (int)Math.Abs(y1-y2);
			   int stepX = x2 < x1 ? 1 : -1;
			   int stepY = y2 < y1 ? 1 : -1;

			   int err = deltaX-deltaY;

			   while (true) {
				   sprite.Plot(x2, y2, color, plotWidth);
				   if (x2 == x1 && y2 == y1) { break; }
				   
				   int e2 = 2 * err;
				   if (e2 > -deltaY) {
					   err = err - deltaY;
					   x2 = x2 + stepX;
				   }

				   if (x2 == x1 && y2 == y1) {
					   sprite.Plot(x2, y2, color, plotWidth);
					   break;
				   }

				   if (e2 < deltaX) {
					   err = err + deltaX;
					   y2 = y2 + stepY;
				   }
			   }
		}

		/// <summary>
		/// Draws a rectangle to the sprite.
		/// </summary>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x1"></param>
		/// <param name="y1"></param>
		/// <param name="x2"></param>
		/// <param name="y2"></param>
		/// <param name="color">The color to draw.</param>
		/// <param name="plotWidth">The pen width.</param>
		/// <param name="fill">True to fill the rectangle.</param>
		public static void DrawRectangle(this int[,] sprite, int x1, int y1, int x2, int y2, int color, int plotWidth = 1, bool fill = false) {
			if (!fill) {
				sprite.DrawLine(x1, y1, x1, y2, color, plotWidth);
				sprite.DrawLine(x1, y2, x2, y2, color, plotWidth);
				sprite.DrawLine(x2, y2, x2, y1, color, plotWidth);
				sprite.DrawLine(x1, y1, x2, y1, color, plotWidth);
			} else {
				if (x1 > x2) {
					Swap(ref x1, ref x2);
				}
				for (int x = x1; x <= x2; x++) {
					sprite.DrawLine(x, y1, x, y2, color, plotWidth);
				}
			}
		}

		/// <summary>
		/// Draws an ellipse to the sprite.
		/// </summary>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x1"></param>
		/// <param name="y1"></param>
		/// <param name="x2"></param>
		/// <param name="y2"></param>
		/// <param name="color">The color to draw.</param>
		/// <param name="plotWidth">The pen width.</param>
		/// <param name="fill">True to fill the ellipse.</param>
		public static void DrawEllipse(this int[,] sprite, int x1, int y1, int x2, int y2, int color, int plotWidth = 1, bool fill = false) {
			if (x2 < x1) { Swap(ref x1, ref x2); }
			if (y2 < y1) { Swap(ref y1, ref y2); }
			
			int hr = (x2 - x1) / 2;
			int kr = (y2 - y1) / 2;
			int h = x1 + hr;
			int k = y1 + kr;

			sprite.DrawEllipseUsingRadius(h, k, hr, kr, color, plotWidth, fill);
		}

		private static void incx(ref int x, ref int dxt, ref int d2xt, ref int t) { x++; dxt += d2xt; t += dxt; }
		private static void incy(ref int y, ref int dyt, ref int d2yt, ref int t) { y--; dyt += d2yt; t += dyt; }

		/// <summary>
		/// Draws a filled ellipse to the sprite.
		/// </summary>
		/// <remarks>Taken from http://enchantia.com/graphapp/doc/tech/ellipses.html.</remarks>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x">The center point X coordinate.</param>
		/// <param name="y">The center point Y coordinate.</param>
		/// <param name="xRadius">The x radius.</param>
		/// <param name="yRadius">The y radius.</param>
		/// <param name="color">The color to draw.</param>
		/// <param name="plotWidth">The pen width.</param>
		/// <param name="fill">True to fill the ellipse.</param>
		public static void DrawEllipseUsingRadius(this int[,] sprite, int x, int y, int xRadius, int yRadius, int color, int plotWidth = 1, bool fill = false) {
			int plotX = 0;
			int plotY = yRadius;
			
			int xRadiusSquared = xRadius * xRadius;
			int yRadiusSquared = yRadius * yRadius;
			int crit1 = -(xRadiusSquared / 4 + xRadius % 2 + yRadiusSquared);
			int crit2 = -(yRadiusSquared / 4 + yRadius % 2 + xRadiusSquared);
			int crit3 = -(yRadiusSquared / 4 + yRadius % 2);
			
			int t = -xRadiusSquared * plotY;
			int dxt = 2 * yRadiusSquared * plotX, dyt = -2 * xRadiusSquared * plotY;
			int d2xt = 2 * yRadiusSquared, d2yt = 2 * xRadiusSquared;

			while (plotY >= 0 && plotX <= xRadius) {
				sprite.Plot(x + plotX, y + plotY, color, plotWidth);
				if (plotX != 0 || plotY != 0) {
					sprite.Plot(x - plotX, y - plotY, color, plotWidth);
				}

				if (plotX != 0 && plotY != 0) {
					sprite.Plot(x + plotX, y - plotY, color, plotWidth);
					sprite.Plot(x - plotX, y + plotY, color, plotWidth);
				}

				if (t + yRadiusSquared * plotX <= crit1 || t + xRadiusSquared * plotY <= crit3) {
					incx(ref plotX, ref dxt, ref d2xt, ref t);
				} else if (t - xRadiusSquared * plotY > crit2) {
					incy(ref plotY, ref dyt, ref d2yt, ref t);
				} else {
					incx(ref plotX, ref dxt, ref d2xt, ref t);
					incy(ref plotY, ref dyt, ref d2yt, ref t);
				}
			}

			if (fill) {
				sprite.FloodFill(x, y, color);
			}
		}

		/// <summary>
		/// Draws a circle to the sprite.
		/// </summary>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="r"></param>
		/// <param name="color">The color to draw.</param>
		/// <param name="plotWidth">The pen width.</param>
		/// <param name="fill">True to fill the circle.</param>
		public static void DrawCircle(this int[,] sprite, int x, int y, int r, int color, int plotWidth = 1, bool fill = false) {
			sprite.DrawEllipseUsingRadius(x, y, r, r, color, plotWidth, fill);
		}

		/// <summary>
		/// Performs a flood fill to the sprite.
		/// </summary>
		/// <param name="sprite">The sprite to draw to.</param>
		/// <param name="x">The starting X coordinate.</param>
		/// <param name="y">The starting Y coordinate.</param>
		/// <param name="color">The color to draw.</param>
		public static void FloodFill(this int[,] sprite, int x, int y, int color) {
			if (x < 0 || y < 0 || x >= sprite.GetLength(0) || y >= sprite.GetLength(1)) { return; }
			if (sprite[x, y] == color) { return; }

			int baseColor = sprite[x, y];
			Stack<Point> s = new Stack<Point>();
			s.Push(new Point(x, y));
			while (s.Count > 0) {
				Point p = s.Pop();
				if (p.X < 0 || p.Y < 0 || p.X >= sprite.GetLength(0) || p.Y >= sprite.GetLength(1)) {
					continue;
				}
				if (sprite[p.X, p.Y] == baseColor) {
					sprite.Plot(p.X, p.Y, color, 1);
					s.Push(new Point(p.X + 1, p.Y));
					s.Push(new Point(p.X - 1, p.Y));
					s.Push(new Point(p.X, p.Y + 1));
					s.Push(new Point(p.X, p.Y - 1));
				}
			}
		}
	}
}
